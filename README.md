# read_tntp_MATLAB

[TransportationNetworks](https://github.com/bstabler/TransportationNetworks)のファイル(`*_net.tntp`, `*_trips.tntp`) をMATLABで読み込むツール

## 使い方

```MATLAB
[G, travel_funs, link_attrs, metadata_net] = read_net_tntp("SiouxFalls_net.tntp"); % ネットワークファイルの読み込み
plot(G); % ネットワークグラフをプロット
travel_funs % リンク旅行時間(BPR関数)
link_attrs % リンクの属性
metadata_net % メタデータ


[OD_table, metadata_trips] = read_trips_tntp("SiouxFalls_trips.tntp"); % OD需要テーブルの読み込み
OD_table % OD需要テーブル
metadata_trips % メタデータ
```

同等のコードが`sample.m`にも置いてあります．


## ドキュメント
### ネットワークファイル(`*_net.tntp`)の読み込み

```MATLAB
[G, travel_funs, link_attrs, metadata] = read_net_tntp(path_to_net_tntp)
```
`path_to_net_tntp`に存在するネットワークファイルを読み込みます．

#### 入力引数

|  |  | データ型 |
|--------------------|------------------------------|----------------------------------|
| `path_to_net_tntp` | ネットワークファイルへのパス | char(ベクトル)またはstring(スカラー) |

#### 出力引数

|  |  | データ型 |
|---------------|-------------------------|---------------------|
| `G` | 有向グラフ | digraphオブジェクト |
| `travel_funs` | リンク旅行時間(BPR関数) | 無名関数のcell配列 |
| `link_attrs` | リンクの属性 | 構造体配列(ベクトル)
| `metadata` | メタデータ | 構造体配列 |

##### `link_attrs`のフィールド

| フィールド名 | エントリ | データ型 |
|------------------|--------------|------------------|
| `init_node` | 始点ノード | double(スカラー) |
| `term_node` | 終点ノード | double(スカラー) |
| `capacity` | 容量 | double(スカラー) |
| `length` | 長さ | double(スカラー) |
| `free_flow_time` | 自由走行時間 | double(スカラー) |
| `B` | B | double(スカラー) |
| `power` | power | double(スカラー) |
| `speed_limit` | Speed limit | double(スカラー) |
| `toll` | 通行料金 | double(スカラー) |
| `link_type` | リンク種別 | double(スカラー) |

各フィールドについての詳細は[TNTP Data format](https://github.com/bstabler/TransportationNetworks#tntp-data-format)を参照のこと．


##### `metadata`のフィールド
必ずしも全てのフィールドが存在するとは限らないので注意．

| フィールド名 | エントリ | データ型 |
|-------------------|--------------------------------------------------------------------------------------------------------------------------------|----------------------------------|
| `number_of_zones` | ゾーン数 | double(スカラー) |
| `number_of_nodes` | ノード数 | double(スカラー) |
| `first_thru_node` | `<FIRST THRU NODE>`(詳細は[TNTP Data format](https://github.com/bstabler/TransportationNetworks#tntp-data-format)を参照のこと) | double(スカラー) |
| `number_of_links` | リンク数 | double(スカラー) |
| `location` | 地点名 | char(ベクトル)またはstring(スカラー) |
| `scenario` | シナリオ | char(ベクトル)またはstring(スカラー) |


### OD需要テーブル(`*_trips.tntp`)の読み込み
```MATLAB
[OD_table, metadata] = read_trips_tntp(path_to_trips_tntp)
```
`path_to_trips_tntp`に存在するOD需要テーブルを読み込みます．

#### 入力引数

|  |  | データ型 |
|--------------------|------------------------------|----------------------------------|
| `path_to_trips_tntp` | OD需要テーブルへのパス | char(ベクトル)またはstring(スカラー) |

#### 出力引数

|  |  | データ型 |
|------------|----------------|--------------|
| `OD_table` | OD需要テーブル(`i`行`j`列の要素はノード`i`からノード`j`への需要) | double(行列) |
| `metadata` | メタデータ | 構造体配列 |

##### `metadata`のフィールド
必ずしも全てのフィールドが存在するとは限らないので注意．

| フィールド名 | エントリ | データ型 |
|-------------------|----------------|----------------------------------|
| `number_of_zones` | ゾーン数 | double(スカラー) |
| `total_od_flow` | ODフローの総和 | double(スカラー) |
| `location` | 地点名 | char(ベクトル)またはstring(スカラー) |



## ライセンス
[CC0](http://creativecommons.org/publicdomain/zero/1.0/)とします．
詳細は`COPYING.txt`を参照のこと．


## TODO
* リンク一般化費用の対応

