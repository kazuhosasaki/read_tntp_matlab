[G, travel_funs, link_attrs, metadata_net] = read_net_tntp("SiouxFalls_net.tntp"); % read *_net.tntp
plot(G); % plot G as digraph
travel_funs % link travel function (BPR function)
link_attrs % link attributes
metadata_net % metadata


[OD_table, metadata_trips] = read_trips_tntp("SiouxFalls_trips.tntp"); % read *_trips.tntp
OD_table % OD demand table
metadata_trips % metadata
