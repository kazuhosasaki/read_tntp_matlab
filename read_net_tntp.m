%{
    read_net_tntp.m

    Written in 2020 by Kazuho Sasaki <kazuho.sasaki.p6@dc.tohoku.ac.jp>

    To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

    You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
%}
function [G, travel_funs, link_attrs, metadata] = read_net_tntp(path_to_net_tntp)
    attrs_schema = { ...
        'init_node', 'term_node', 'capacity', 'length', 'free_flow_time', 'B', 'power', 'speed_limit', 'toll', 'type' ...
    };
    metadata_schema = { ...
       {'<NUMBER OF ZONES> %d ', 'number_of_zones', false}, ...
       {'<NUMBER OF NODES> %d ', 'number_of_nodes', false}, ...
       {'<FIRST THRU NODE> %d ', 'first_thru_node', false}, ...
       {'<NUMBER OF LINKS> %d ', 'number_of_links', false}, ...
       {'(?<=<LOCATION>\s*").*(?=")', 'location', true}, ...
       {'(?<=<SCENARIO>\s*").*(?=")', 'scenario', true}
    };

    metadata = {};
    fd = fopen(path_to_net_tntp);
    while ~feof(fd)
        l = strtrim(fgetl(fd));
        if strcmp(l, '<END OF METADATA>')
            break;
        else
           for item_i = metadata_schema
               item = item_i{1};
               if item{3}
                   a = regexp(l, item{1}, 'match', 'once');
               else
                   a = sscanf(l, item{1});
               end
               if ~isempty(a)
                   metadata.(item{2}) = a;
                   break;
               end
           end
        end
    end

    s = zeros(1, metadata.number_of_links);
    t = zeros(1, metadata.number_of_links);
    travel_funs = cell(1, metadata.number_of_links);
    %link_attrs = struct('free_flow_time', cell(1, metadata.number_of_links), 'capacity', cell(1, metadata.number_of_links));
    for attr = attrs_schema
        link_attrs(1, metadata.number_of_links).(attr{1}) = [];
    end

    idx = 1;
    while ~feof(fd)
        l = strtrim(fgetl(fd));
        if strncmp(l, '~', 1) || isempty(l)
            continue;
        else
            a = sscanf(l, ' %d %d %lf %lf %lf %lf %lf %lf %lf %d ; ', 10);
            s(idx) = a(1);
            t(idx) = a(2);
            for attr_i = 1:length(attrs_schema)
                link_attrs(idx).(attrs_schema{attr_i}) = a(attr_i);
            end
            travel_funs{idx} = @(x) ((a(5))*(1 + (a(6))*(x/(a(3)))^(a(7)))); % BPR function
            idx = idx + 1;
        end
    end
    fclose(fd);
    G = digraph(s, t);
end
