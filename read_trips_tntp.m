%{
    read_trips_tntp.m

    Written in 2020 by Kazuho Sasaki <kazuho.sasaki.p6@dc.tohoku.ac.jp>

    To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

    You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
%}
function [OD_table, metadata] = read_trips_tntp(path_to_trips_tntp)
    metadata_schema = { ...
       {'<NUMBER OF ZONES> %d ', 'number_of_zones', false}, ...
       {'<TOTAL OD FLOW> %d ', 'total_od_flow', false}, ...
       {'(?<=<LOCATION>\s*").*(?=")', 'location', true}
    };

    metadata = {};
    fd = fopen(path_to_trips_tntp);
    while ~feof(fd)
        l = strtrim(fgetl(fd));
        if strcmp(l, '<END OF METADATA>')
            break;
        else
           for item_i = metadata_schema
               item = item_i{1};
               if item{3}
                   a = regexp(l, item{1}, 'match', 'once');
               else
                   a = sscanf(l, item{1});
               end
               if ~isempty(a)
                   metadata.(item{2}) = a;
                   break;
               end
           end
        end
    end

    OD_table = zeros(metadata.number_of_zones);
    while ~feof(fd)
        l = strtrim(fgetl(fd));
        if strncmp(l, '~', 1) || isempty(l)
            continue;
        else
            a = sscanf(l, "Origin %d");
            if ~isempty(a)
                o = a; % origin node number
                continue;
            end

            A = sscanf(l, "%d : %lf ;", [2 Inf]);
            if ~isempty(A)
                for a = A
                    d = a(1); % destination node number
                    flow = a(2); % OD demand
                    OD_table(o, d) = flow;
                end
                continue;
            end
        end
    end
    fclose(fd);
end
